# Web App S3

## Reason why this website was born 
I created this website to advertise my appartment for rent. I think It's would be great when we can apply technology to solve real-world problem (In my case, I don't have to hire another agent to create my website, that helps save money :moneybag:)

## Architect
![architect](/images/website-s3-cloudfront.png)

This website is hosted on AWS S3, and take advantage of AWS CloudFront for CDN. For secure S3 bucket access, The bucket is not made public, and only AWS CloudFront endpoint can access the content.

Every time I update new features for my website on my local computer, I will update S3 bucket by using `aws s3 sync` command. We might need implement CloudFront invalidation to make it update new content.

You can visit my website here: [https://dgkcnpfqvbtac.cloudfront.net](https://dgkcnpfqvbtac.cloudfront.net)
